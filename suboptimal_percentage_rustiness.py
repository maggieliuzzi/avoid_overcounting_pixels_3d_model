import numpy as np


# Defining order to avoid double-counting the distance between A and B (eg. AB and BA).
def get_index(a, b):
    if (a[0]+a[1]+a[2])>=(b[0]+b[1]+b[2]):
        return (tuple(a), tuple(b))
    else:
        return (tuple(b), tuple(a))


# Estimating the distance between two pixels
def fast_distance(a, b):
    # a[0] is the x value of one pixel and b[0] is the x value of the other.
    # a[1] is the y value of one pixel and a[1] is the y value of the other.
    return (a[0]-b[0])**2 + (a[1]-b[1])**2 + (a[2]-b[2])**2


''' Definitely needs optimisation: '''
# Finding points which are closer than 2cm to each other and adding them to a list.
# A heuristic is provided to reduce the number of comparisons performed.
# Searching for a point that represents a rusty area in both pictures. Then checking if they are within 5m of each other.
# If so, the pictures are considered to be in the same area and distances are calculated between all pixels.
def get_mergeable(pictures, closeness = 1):



    '''
    mergeable = []
    fast_closeness = closeness ** 2
    for i, left_picture in enumerate(pictures[:-2]):
        left_rusty_pixel = sorted(left_picture, key=lambda x: x[2])[-1]
        for right_picture in pictures[i:]:
          right_rusty_pixel = sorted(right_picture, key=lambda x: x[2])[-1]
          square_distance = fast_distance(left_rusty_pixel[:-3], right_rusty_pixel[:-3])
          if square_distance <= fast_closeness:
              for left_pixel in left_picture:
                  for right_pixel in right_picture:
                      square_distance = fast_distance(left_pixel[:-3], right_pixel[:-3])
                      if square_distance <= 0.0004 and square_distance > 0:
                          index = get_index(left_pixel[:-3], right_pixel[:-3])
                          mergeable.append(index)
    '''
    return mergeable


# Computing an estimate of the percentage of rusty points out of total points.
# Close-by points (i.e. points within 2cm of each other) are merged.
def perc_rustiness(pictures):
    point_cloud = {}
    merge_points = get_mergeable(pictures)
    for picture in pictures:
        for pixel in picture:
            x, y, z = pixel[3], pixel[4], pixel[5]
            if (x, y, z) not in point_cloud:
                point_cloud[(x, y, z)] = [pixel]
            else:
                point_cloud[(x, y, z)].append(pixel)
    rusty_points = 0
    for point_pair in merge_points:
        del(point_cloud[point_pair[0]])
    for index, points in point_cloud.items():
        if len(points) > 1:
            rusty_votes = 0
            for point in points:
                rusty_votes += point[2]
            if rusty_votes >= len(points):
                rusty_points += 1
        else:
            rusty_points += points[0][2]
    area = rusty_points / float(len(point_cloud.items()))
    print(area)
    return area


# Dummy data
pixel1 = [12, 142,0, 3.61, 4.32, 5.61]
pixel2 = [123, 14,0, 13.6, 2.3, 3.81]
pixel3 = [124, 14,1, 13.59, 2.29, 3.82]
pixel4 = [1245, 921, 1, 21.3, 7.83, 0.11]
pixel5 = [1246, 922, 1, 21.4, 7.83, 0.10]
pixel6 = [1246, 923, 0, 21.4, 7.90, 0.10]
photo1 = np.array([pixel1])
photo2 = np.array([pixel2, pixel3])
photo3 = np.array([pixel4, pixel5, pixel6])


perc_rustiness([photo1, photo2, photo3])
