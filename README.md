The script (percentage_rustiness.py) calculates the percentage of rustiness in an area by storing all pixels from all overlapping photos on a K-D Tree and then avoiding overcounting close enough pixels when traversing the tree. 

