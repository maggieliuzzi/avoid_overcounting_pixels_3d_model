import numpy as np
import pprint


def create_dictionary(photos):
    pixel_dict = {}
    for photo in photos:
        for pixel in photo:
            x, y, z = pixel[3], pixel[4], pixel[5]
            if (x, y, z) not in pixel_dict:
                pixel_dict[(x, y, z)] = [pixel]
            else:
                pixel_dict[(x, y, z)].append(pixel)
    return pixel_dict


def build_kdtree(points, depth=0):
    n = len(points)
    if n <= 0:
        return None
    k = 3
    axis = depth % k
    sorted_points = sorted(points, key=lambda point: point[axis])
    return {
        'point': sorted_points[n // 2],
        'left': build_kdtree(sorted_points[:n // 2], depth + 1),
        'right': build_kdtree(sorted_points[n // 2 + 1:], depth + 1)
    }


# Estimating the distance between two pixels
def fast_distance(a, b):
    # a[0] is the x value of one pixel and b[0] is the x value of the other.
    return (a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2 + (a[2] - b[2]) ** 2


def kdtree_range_search(root, point, duplicate_points, neighbours=0, depth=0, max_dist=0.02):
    if root is None:
        return
    k = 3
    axis = depth % k
    next_branch = None
    if fast_distance(point, root['point']) < (max_dist ** 2) and point != root['point']:
        duplicate_points[point] = root['point']
    if point[axis] < root['point'][axis]:
        next_branch = root['left']
    else:
        next_branch = root['right']
    return kdtree_range_search(next_branch, point, duplicate_points, neighbours, depth + 1)


def traverse_tree(kdtree, root, duplicate_points, valid_points):
    kdtree_range_search(root, kdtree['point'], duplicate_points)
    try:
        if duplicate_points[kdtree['point']]:
            pass
    except KeyError:
        valid_points.append(kdtree['point'])
    if kdtree['left'] is not None:
        traverse_tree(kdtree['left'], root, duplicate_points, valid_points)
    if kdtree['right'] is not None:
        traverse_tree(kdtree['right'], root, duplicate_points, valid_points)


def perc_rustiness(photos):
    pp = pprint.PrettyPrinter()

    # Creating a dictionary out of original list of pixels
    pixel_dict = create_dictionary(photos)

    # Building a KDTree (I know it's easy to do with a library, but I wanted to code it manually)
    kdtree = build_kdtree(pixel_dict)
    # pp.pprint(kdtree)

    # Traversing the tree and separating valid and duplicate points, the latter being those without duplicates, i.e. no other points within 0.02m
    duplicate_points = {}
    valid_points = []
    traverse_tree(kdtree, kdtree, duplicate_points, valid_points)

    # Checking if rusty or good for each valid point. Since our dictionary is structured based on distinct (> 0.02m) pixels,
    # we manage potential differences in classification by creating a voting system.
    rusty_count = 0
    for key, value in duplicate_points.items():
        pixel_dict[value] += pixel_dict[key]
    for valid_point in valid_points:
        rusty_vote = 0
        for valid_pixel in pixel_dict[valid_point]:
            rusty_vote += valid_pixel[2]
        # We use the decision of the majority of the pixels considered duplicates of a distinct pixel.
        if rusty_vote >= len(pixel_dict[valid_point]) / 2:
            rusty_count += 1
    perc = rusty_count / len(valid_points)
    return perc


if __name__ == '__main__':
    # Dummy data
    pixel1 = [12, 142, 0, 13.61, 4.32, 5.61]
    pixel2 = [12, 142, 0, 13.61, 4.32, 5.61]
    pixel3 = [123, 14, 0, 13.6, 2.3, 3.81]
    pixel4 = [124, 14, 1, 13.59, 2.29, 3.82]
    pixel5 = [1245, 921, 1, 2.3, 7.83, 0.11]
    pixel6 = [1246, 922, 1, 21.4, 7.83, 0.10]
    pixel7 = [1246, 923, 0, 21.4, 7.90, 0.10]
    pixel8 = [1247, 923, 0, 4.01, 7.01, 1.01]
    pixel9 = [1247, 923, 0, 1.01, 2.01, 3.01]
    pixel10 = [1247, 923, 0, 4.01, 5.01, 6.01]
    pixel11 = [1247, 923, 0, 7.01, 8.01, 9.01]
    pixel12 = [1247, 923, 0, 10.01, 11.01, 12.01]
    pixel13 = [1247, 923, 0, 10.01, 11.01, 12.01]
    photo1 = np.array([pixel1, pixel13])
    photo2 = np.array([pixel2, pixel3, pixel4])
    photo3 = np.array([pixel5, pixel6, pixel7, pixel8, pixel9, pixel10, pixel11, pixel12])

    # Calculating percentage of rustiness in area
    rust_percent = perc_rustiness([photo1, photo2, photo3])
    print(f'{rust_percent:.2f}') # (Requires python3)